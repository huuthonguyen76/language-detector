package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.json.simple.JSONArray;

import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileBuilder;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.profiles.LanguageProfileWriter;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;

public class test_main{
	public static void main(String[] args) throws Exception {
        // buildProfile(args[0], args[1]);
        // test();
        evaluate();
        // buildProfile("id", "/Users/nguyenhuutho/Desktop/workspace/neural-network/id_compound.txt");
	}

    private static void test() throws Exception {
        //load all languages:
        List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();

        //build language detector:
        LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard()).withProfiles(languageProfiles).build();

        //create a text object factory
        TextObjectFactory textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();

        TextObject textObject = textObjectFactory.forText("my text");
        Optional<LdLocale> lang = languageDetector.detect(textObject);
        System.out.println(lang);
    }

    private static void evaluate() throws Exception {
        //load all languages:
        List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();
        //build language detector:
        LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard()).withProfiles(languageProfiles).build();

        //create a text object factory
        TextObjectFactory textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();

        JSONArray listGoldLabel = new JSONArray();
        JSONArray listPredLabel = new JSONArray();

        long total = 0;
        long start = System.currentTimeMillis();
        for (File folder: getListFile("/Users/nguyenhuutho/Desktop/workspace/neural-network/zipfile/")) {
            String folder_name = folder.getName();
            String lang = folder_name.split("_")[0];

            int counter = 0;
            for (File file: getListFile("/Users/nguyenhuutho/Desktop/workspace/neural-network/zipfile/" + lang + "_file/")) {
                total += 1;
                String content = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
                
                if (content.length() < 200) {
                    continue;
                }
                TextObject textObject = textObjectFactory.forText(content);
                Optional<LdLocale> lang_obj = languageDetector.detect(textObject);
                if (lang_obj.isPresent()) {
                    listGoldLabel.add(lang);
                    listPredLabel.add(lang_obj.get().toString());
                } else {
                    listGoldLabel.add(lang);
                    listPredLabel.add("unknown");
                }
                counter++;

                 if (counter == 1000) {
                     break;
                 }
            }
        }
        long end = System.currentTimeMillis();
        System.out.println(total);
        System.out.println(end - start);

        BufferedWriter writer = new BufferedWriter(new FileWriter("result.txt"));
        writer.write(listPredLabel.toString());
        writer.write("\n");
        writer.write(listGoldLabel.toString());
        writer.close();
    }

    private static File[] getListFile(String path) throws Exception{
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        return listOfFiles;
    }

    private static void buildProfile(String lang, String fileName) throws Exception {
        String data = readFileAsString(fileName);
        String[] listPart = data.split("\n");

        System.out.println("Reading training file");
        TextObjectFactory textObjectFactory = CommonTextObjectFactories.forIndexing();
        TextObject inputText = textObjectFactory.create();

        for (String part: listPart) {
            inputText.append(part);
        }

        LanguageProfile languageProfile = new LanguageProfileBuilder(lang).ngramExtractor(NgramExtractors.standard())
                .addText(inputText)
                .build();

        File file = new File("./");
        System.out.println("Generating profile ...");
        new LanguageProfileWriter().writeToDirectory(languageProfile, file);

        System.out.println("Done!");
    }

    private static String readFileAsString(String fileName) throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }
}


